#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from spinney.thermodynamics.chempots import Range, get_chem_pots_conditions

if __name__ == '__main__':
    data = 'formation_energies.txt'

    equality_compounds = ['GaN']
    order = ['Ga', 'N', 'Mg']
    parsed_data= get_chem_pots_conditions(data, order, equality_compounds)         
    # prepare Range instances
    crange = Range(*parsed_data[:-1])

    ranges = crange.variables_extrema
    print(ranges)

    crange.set_compound_dict(parsed_data[-1])
    # let's use some pretty labels in the plot
    # the order of the axes  must follow the order used for get_chem_pots_conditions
    labels = [r'$\Delta \mu_{Ga}$ (eV)', r'$\Delta \mu_{N}$ (eV)',
              r'$\Delta \mu_{Mg}$ (eV)']
    crange.plot_feasible_region_on_plane([1,2], x_label=labels[1],
                                         y_label=labels[2],
                                         title='GaN Ga-rich',
                                         save_plot=False)

    print(crange.variables_extrema_2d)
    
    crange.plot_feasible_region_on_plane([0,2], x_label=labels[0],
                                         y_label=labels[2],
                                         title='GaN N-rich',
                                         x_val_min=-3, y_val_min=-4,
                                         save_plot=False)

    print(crange.variables_extrema_2d)
