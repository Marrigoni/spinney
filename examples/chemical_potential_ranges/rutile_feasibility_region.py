#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from spinney.thermodynamics.chempots import Range, get_chem_pots_conditions

if __name__ == '__main__':
    data = 'formation_energies.txt'

    equality_compounds = ['TiO2_rutile']
    order = ['O', 'Ti']
    parsed_data= get_chem_pots_conditions(data, order, equality_compounds)         
    # prepare Range instances
    crange = Range(*parsed_data[:-1])

    print(crange.variables_extrema)

    crange.set_compound_dict(parsed_data[-1])
    # let's use some pretty labels in the plot
    # the order of the axes  must follow the order used for get_chem_pots_conditions
    labels = [r'$\Delta \mu_{O}$ (eV)', r'$\Delta \mu_{Ti}$ (eV)']
    crange.plot_feasible_region_on_plane([0,1], x_label=labels[0],
                                         y_label=labels[1],
                                         title='Rutile phase', save_plot=True)

