Example for the Ga vacancy in charge state -3 in GaAs, modelled using a 3x3x3 expansion
of the conventional FCC cell.

Needed data:

LOCPOT_prist : VASP electrostatic potential on the 3D grid for the pristine supercell.
LOCPOT_def   : VASP electrostatic potential on the 3D grid for the defective system.
PARCHG_def   : electronic charge density of the defect-induced level sampled on a 3D grid.
               This file is needed to fit the model distribution to the defect charge density.
