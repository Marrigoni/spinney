The :program:`Spinney` package
******************************

:program:`Spinney` is a Python package dedicated to the study of point defects in solids.
In particular, the code is designed to assist with the post-processing of first-principles calculations.

The aim of the package is to perform the several steps required in order to obtain meaningful
quantities and properties concerning materials with point defects
from the output files produced by first-principles codes.

These are some of the most relevant tasks that :program:`Spinney` is able to perform:
 
- Compute, from external data (taken for example from online repositories), the ranges of validity 
  for the elements chemical potentials according to thermodynamic constraints;
- Calculate the correction energy due to electrostatic finite-size-effects in charged supercells
  using state-of-the-art schemes;
- Calculate defect formation energies and transition levels;
- Calculate defects and carriers concentrations.

In developing :program:`Spinney`, we aimed for a great code flexibility. For this reason,
all the core routines take as argument either built-in Python objects or data structures
ubiquitous in scientific computing, such as **NumPy** arrays.

This makes possible the integration of :program:`Spinney` with any first-principle code,
as the core routines can be used to write a proper interface for the relevant computer package.

For convenience, :program:`Spinney` offers also a higher-level object for a direct calculation of
the defect formation energy from the result of first-principles calculations.
Such property is arguably the most relevant descriptor characterizing a point defect.
For technologically-relevant materials, such as insulators and semiconductors, the presence 
of charged defects is ubiquitous. 
The :class:`~spinney.structures.pointdefect.PointDefect` class offers a handy way
for calculating the defect formation energy, including correction terms for dealing with
electrostatic finite-size effects using state-of-the-art schemes.
For convenience, such class can be initialized by a :class:`ase.Atoms` instance of the
**ASE** library. 

Reading and processing the output of first principles calculations describing
several different point defects in the same host material is straightforward through
the :class:`~spinney.structures.defectivesystem.DefectiveSystem` class. 
