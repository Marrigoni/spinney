Welcome to Spinney's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   general
   installation
   case_study
   tutorial/index
   releasenotes
   api
   bibliography
   contacts
