API reference
*************

.. automodule:: spinney
   :members:

General high-level interface  for point-defect calculations
===========================================================

- :mod:`spinney.structures.pointdefect`

- :mod:`spinney.structures.defectivesystem`

The :mod:`pointdefect` module
-----------------------------

.. automodule:: spinney.structures.pointdefect
   :members:

The :mod:`defectivesystem` module
---------------------------------

.. automodule:: spinney.structures.defectivesystem
   :members:

Determination of the possible values of equilibrium chemical potentials
=======================================================================

- :mod:`spinney.thermodynamics.chempots`

The :mod:`chempots` module
--------------------------

.. automodule:: spinney.thermodynamics.chempots
   :members:

Correction schemes for electrostatic finite-size effects in supercells
======================================================================

- :mod:`spinney.defects.kumagai`
- :mod:`spinney.defects.fnv`

The :mod:`kumagai` module
-------------------------

.. automodule:: spinney.defects.kumagai
   :members:

The :mod:`fnv` module
---------------------

.. automodule:: spinney.defects.fnv
   :members:

Calculation of equilibrium defect properties
============================================

- :mod:`spinney.defects.diagrams`
- :mod:`spinney.defects.concentration`

The :mod:`diagrams` module
--------------------------

.. automodule:: spinney.defects.diagrams
   :members:

The :mod:`concentration` module
-------------------------------

.. automodule:: spinney.defects.concentration
   :members:

General-purpose tools
=====================

- :mod:`spinney.tools.formulas`
- :mod:`spinney.tools.reactions`

The :mod:`formulas` module
--------------------------

.. automodule:: spinney.tools.formulas
   :members:

The :mod:`reactions` module
---------------------------

.. automodule:: spinney.tools.reactions
   :members:

Support for first-principles codes
==================================

- :mod:`spinney.io.vasp`
- :mod:`spinney.io.wien2k`

The :mod:`io.vasp` module
-------------------------

.. automodule:: spinney.io.vasp
   :members:

The :mod:`io.wien2k` module
---------------------------

.. automodule:: spinney.io.wien2k
   :members:

