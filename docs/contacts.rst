Contact
*******

E-mail
------
The developers of :program:`Spinney` can be reached through these e-mail addresses:

    - marco.arrigoni@tuwien.ac.at, marco.arrigoni@outlook.de
    - georg.madsen@tuwien.ac.at

Gitlab
------
:program:`Spinney` is hosted on Gitlab on the following web page:

    - https://gitlab.com/Marrigoni/spinney
