Installation
************

Setup
=====
:program:`Spinney` is available one the
`Python Package Index <https://pypi.org/>`_,
the most simple way to install it is using `pip`:

::

    pip install spinney

After the installation is completed, you can run some tests
in order to see if the installation was successfull.

To do so, run the command:

::

    spinney-run-tests

The :program:`Spinney` package is also hosted on
`Gitlab <https://gitlab.com/Marrigoni/spinney>`_.

In this case, to install the package, create a new directory and
clone the repository. From the root directory, run the command:

::

    python3 setup.py install

And to check if the installation was successfull, type:

::

    python3 setup.py test

.. note::

    We recommend to install :program:`Spinney` within a Python virtual environment,
    such as those created by `Virtualenv <https://virtualenv.pypa.io>`_.
 
Requirements
============
:program:`Spinney` requires `Python3 <https://www.python.org>`_
and the following libraries:

- `NumPy <https://www.numpy.org>`_ version  1.12 or newer;
- `SciPy <https://www.scipy.org>`_ version 1.4 or newer;
- `Pandas <https://pandas.pydata.org/>`_ version 0.25 or newer;
- `Matplotlib <https://matplotlib.org/>`_ version 3.1.0 or newer;
- `ASE <https://wiki.fysik.dtu.dk/ase/>`_ version 3.18 or newer. 

  .. warning::

      :program:`Spinney`
      is **not** compatible with :mod:`ASE` versions
      older than the `3.18` one.
