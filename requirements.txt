sphinxcontrib-bibtex==1.0.0
numpy>=1.15.4
scipy>=1.4.0
matplotlib>=3.0.2
pandas>=0.25
ase>=3.18.0
